#Página 13 del documento en adelante.

l = [22, True, "una lista", [1,2]];
var = l[0]; #var vale, ahora 22
print("var vale, ahora 22: " + str(var));

l[0] = 99 # l valdrá [99, True]
var = l[0]; #var vale, ahora 99
print("var vale, ahora 99: " + str(var));


var = l[1:]; # var vale [True, “una lista”]
var = l[:2]; # var vale [99, True]
var = l[:]; # var vale [99, True, “una lista”]
var = l[::2]; # var vale [99, “una lista”]


t = (1, 2, True, "python"); #tupla
var = t[0]; # var es 1
var = t[0:2]; # var es (1, 2)


# clave -> valor
d = {"Love Actually": "Richard Curtis", "Kill Bill": "Tarantino", "Amélie": "Jean-Pierre Jeunet"};

d["Love Actually"]; # devuelve “Richard Curtis”
d["Kill Bill"] = "Quentin Tarantino"; # valor reasignado
