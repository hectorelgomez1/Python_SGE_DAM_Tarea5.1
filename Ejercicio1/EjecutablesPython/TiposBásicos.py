entero = 23;
print (type(entero));
# type(entero) devolvería int

longNumber = int(23);
print (type(longNumber));
# type(entero) devolvería long
# esto ha cambiado respecto al ejemplo del manual. en python 3 es con int, no con Long.

# 23 en octal
entero = oct(23);
print (type(entero));
print(entero);


# 23 en binario
entero = bin(23);
print (type(entero));
print(entero);


# 23 en hexadecimal, todo esto ha cambiado respecto al ejemplo
entero = hex(23);
print (type(entero));
print(entero);

# No ejecuto estos ejemplos porque es muy diferente de Python 2 a 3:
#real = 0.2703;
#real = 0.1e-3 (0.0001);
#print (type(real));
#print(real);


#!/usr/bin/env python
#ejemplo de operador de cálculo básico:
a = 3;
b = 12;
suma = a+b;
print ("\n\n La suma de " + str(a) + " + " + str(b) + " es igual a: "+str(suma));


#ejemplo de operador condicional básico y lógico:
a = 1; #2
if (a==1 & 1==1) :
	print ("\n\n " + str(a) + " es igual a 1 y 1 es igual a 1. ¡Felicidades!");
else :
	print ("\n\n " + str(a) + " NO es igual a 1 aunque 1 es igual a 1. ¡Muy mal!");



#ejemplo de cadena y de booleano:
cadena = "Hola Holita.";
a = True; #False
if (a) :
	print ("\n\n " + cadena + " a es cierto. ¡Felicidades!");
else :
	print ("\n\n " + cadena + " a es falso");


#Página 12 del documento


