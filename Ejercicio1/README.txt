He estructurado la resolución del ejercicio de la siguiente manera:

-Documento "Ejercicio1.odt" -> Capturas de pantalla de las ejecuciones de código y comentarios personales sobre el ejercicio.
-Presentación "python.pptx" -> Ejercicio a realizar.
-Carpeta "CapturasDePantallaUsadas" -> Aquí he ido guardando todas las capturas que aparecen en el documento, por si se rompe o algo.
-Carpeta "EjecutablesPython" -> Todos los ejecutables con el código fuente de los ejercicios. Preparados para la versión 3 de python. Están comentados, con lo que mejor darles un vistacito pues no se lee todo en la resolución.