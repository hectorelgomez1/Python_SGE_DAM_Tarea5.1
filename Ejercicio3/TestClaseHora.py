from ClaseHora import Hora;

miHora = Hora();
print (miHora.printHora());

miHora = Hora( 8, 36, 42);
print (miHora.printHora());

miHora = miHora.leerHora();
print (miHora.printHora());

segundos = miHora.a_segundos();
print ("Segundos desde medianoche: "+ str(segundos) );


segundos = input("Escribe los segundos a sumar a medianoche: ");
miHora = miHora.de_segundos(segundos);
print (miHora.printHora());
