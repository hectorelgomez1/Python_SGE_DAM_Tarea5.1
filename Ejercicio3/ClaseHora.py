class Hora:
	def __init__(self, horas=00, minutos=00, segundos=00) :
		#Constructor predeterminado con el 00:00:00 como hora por defecto. En el constructor se podrán indicar horas, minutos y segundos.
		self.horas = horas;
		self.minutos = minutos;
		self.segundos = segundos;
		self.__valida();

	def __str__(self) :
		return str(self.horas) + ":" + str(self.minutos) + ":" + str(self.segundos);

	def leerHora(self) :
		#pedirá al usuario las horas, los minutos y los segundos.
		self.horas = input("Escribe la Hora: ");
		self.minutos = input("Escribe los minutos: ");
		self.segundos = input("Escribe los segundos: ");
		self.__valida();
		return self;

	def __valida(self) : # según algunos tutoriales el doble __ lo hace privado... otros dicen lo contrario.
		#comprobará si la hora es correcta; si no lo es la ajustará. Será un método auxiliar (privado) que se llamará en el constructor parametrizado y en leer().
		if self.horas == "" or int(self.horas)>=24 or int(self.horas)<0 :
			self.horas = 00;
		else :
			self.horas = self.horas;
		if self.minutos == "" or int(self.minutos)>=60 or int(self.minutos)<0 :
			self.minutos = 00;
		else :
			self.minutos = self.minutos;
		if self.segundos == "" or int(self.segundos)>=60 or int(self.segundos)<0 :
			self.segundos = 00;
		else :
			self.segundos = self.segundos;
		return self;

	def printHora(self):
		#mostrará la hora (07:03:21).
		return str(self.horas) + ":" + str(self.minutos) + ":" + str(self.segundos);

	def a_segundos(self):
		#devolverá el número de segundos transcurridos desde la medianoche.
		segundos = int( int(self.horas) * 3600) + int( int(self.minutos) * 60) + int(self.segundos);
		return segundos;

	def de_segundos(self, segundos):
		#hará que la hora sea la correspondiente a haber transcurrido desde la medianoche los segundos que se indiquen.
		#int( int(self.horas) * 3600) + int( int(self.minutos) * 60) + int(self.segundos);
		self.horas = int(segundos)/3600;
		#divmod(x, y)
		segundos = int(segundos)%3600;
		self.minutos = int(segundos)/60;
		segundos = int(segundos)%60;
		self.segundos = int(segundos);
		return self;



	def convierteASegundos(self):
		minutos = self.horas * 60 + self.minutos;
		segundos = self.minutos * 60 + self.segundos;
		return segundos;

	def incrementa(self, segs) :
		segs = segs + self.segundos;
		self.horas = self.horas + segs/3600;
		segs = segs % 3600;
		self.minutos = self.minutos + segs/60;
		segs = segs % 60;
		self.segundos = segs;

	def haceHora(segs) :
		hora = Hora();
		hora.horas = segs/3600;
		segs = segs - hora.horas * 3600;
		hora.minutos = segs/60;
		segs = segs - hora.minutos * 60;
		hora.segundos = segs;
		return hora;
