from ClaseEmpleado import Empleado;


#Crea una clase Empleado que modele la información que una empresa mantiene sobre cada empleado: NIF, sueldo base, pago por hora extra, horas extra realizadas en el mes, tipo (porcentaje) de IRPF, casado o no y número de hijos. Al crear un empleado se podrá proporcionar, si se quiere, el número de DNI.

yoMismo = Empleado();
print (yoMismo.printHora());

miHora = Hora( 8, 36, 42);
print (miHora.printHora());

miHora = miHora.leerHora();
print (miHora.printHora());

segundos = miHora.a_segundos();
print ("Segundos desde medianoche: "+ str(segundos) );


segundos = input("Escribe los segundos a sumar a medianoche: ");
miHora = miHora.de_segundos(segundos);
print (miHora.printHora());
