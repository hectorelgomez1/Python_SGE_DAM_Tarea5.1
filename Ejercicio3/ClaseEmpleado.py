class Empleado:

#Crea una clase Empleado que modele la información que una empresa mantiene sobre cada empleado: NIF, sueldo base, pago por hora extra, horas extra realizadas en el mes, tipo (porcentaje) de IRPF, casado o no y número de hijos. Al crear un empleado se podrá proporcionar, si se quiere, el número de DNI.

	def __init__(self, NIF=66666666X, sueldoBase=00, pagoHoraExtra=00, horasExtraRealizadasMes=00, tipoIRPF=15, casado=false, hijos=0) :
		self.NIF = NIF;
		self.sueldoBase = sueldoBase;
		self.pagoHoraExtra = pagoHoraExtra;
		self.horasExtraRealizadasMes = horasExtraRealizadasMes;
		self.tipoIRPF = tipoIRPF;
		self.casado = casado;
		self.hijos = hijos;
		#self.__valida();

	def leerEmpleado(self) :
		#NIF=66666666X, sueldoBase=00, pagoHoraExtra=00, horasExtraRealizadasMes=00, tipo=15, casado=false, hijos=0
		self.NIF = input("Escribe NIF: ");
		self.sueldoBase = input("Escribe sueldoBase: ");
		self.pagoHoraExtra = input("Escribe pagoHoraExtra: ");
		self.horasExtraRealizadasMes = input("Escribe horasExtraRealizadasMes: ");
		self.tipo = input("Escribe tipo: ");
		self.casado = input("Escribe casado: ");
		self.hijos = input("Escribe hijos: ");
		self.__valida();
		return self;

	def complementoHoras(self) :
		#devolver el complemento correspondiente a las horas extra realizadas.
		complemento = int(self.pagoHoraExtra)*int(self.horasExtraRealizadasMes);
		return complemento;

	def sueldoBruto(self) :
		#devolver el sueldo bruto.
		sueldoBruto = int(self.leerEmpleado()) + self.sueldoBase;
		return sueldoBruto;

	def retencionesIRPF(self) :
		#devolver las retenciones (IRPF) a partir del tipo, teniendo en cuenta que el porcentaje de retención que hay que aplicar es el tipo menos 2 puntos si el empleado está casado y menos 1 punto por cada hijo que tenga; el porcentaje se aplica sobre todo el sueldo bruto.
		tipo = int(self.tipoIRPF) + int(self.hijos);
		if self.casado == true :
			tipo = int(tipo) + 2;
		retenciones = int(self.sueldoBruto())*(int(tipo)*0.1);
		return retenciones;

	def printEmpleado(self) :
		#print visualiza la información básica del empleado (sueldo base, complemento por horas extra, sueldo bruto, retención de IRPF y sueldo neto).
		sueldoBruto = int(self.leerEmpleado()) + self.sueldoBase;
		return sueldoBruto;

	def imprimeTodo(self) :
		#un método especial imprime_todo() muestra toda la información del empleado.
		sueldoBruto = int(self.leerEmpleado()) + self.sueldoBase;
		return sueldoBruto;

	def copiaEmpleado(self) :
		#clona el objeto.
		nuevoEmpleado = self;
		return nuevoEmpleado;


