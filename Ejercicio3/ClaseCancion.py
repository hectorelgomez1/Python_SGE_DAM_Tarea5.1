class Cancion:

#Desarrolla una clase Cancion con los siguientes atributos:
	_titulo = ""; #• _titulo: una variable String que guarda el título de la canción.
	_autor = ""; #• _autor: una variable String que guarda el autor de la canción.
	_duracion = 00; #• _duracion: tiempo en segundos de la canción.

	def __init__(self, titulo="NombreCancion", autor="Anonimo") :
		#El constructor que recibe como parámetros el título y el autor de la canción (por este orden)
		self._titulo = titulo;
		self._autor = autor;

	def dame_titulo(self) :
		#devuelve el título de la canción.
		return self._titulo;

	def dame_autor(self) :
		#devuelve el autor de la canción.
		return self._autor;

	def pon_titulo(titulo) : 
		#establece el título de la canción.
		self._titulo = titulo;
		return;

	def pon_autor(autor) :
		#establece el autor de la canción.
		self._autor = autor;
		return;


#Convierte los métodos anteriores en propiedades.
