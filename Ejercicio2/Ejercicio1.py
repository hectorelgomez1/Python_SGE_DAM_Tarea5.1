import math

#Ejercicio 1:
A = 'a'; # he cambiado el pseudocódigo <- por = porque este no existe en python
print (A);

ONCE =  11;
print (ONCE);

r = 50;
print (r);

s = 6;
print (s);

#t = trunc(r/s);
t = math.trunc(-3.5) # work as ceil
print (t);

# s <- r mod(s);  esto es de otra versión de python. Deprecated.
s = r % s;
print(s);

r = ONCE - t*s;
print(r);
